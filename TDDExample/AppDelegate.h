//
//  AppDelegate.h
//  TDDExample
//
//  Created by user on 18.07.13.
//  Copyright (c) 2013 ScienceSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
